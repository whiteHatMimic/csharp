$(document).ready(function(){
  //滑鼠移到 google 圖片
  $("img.logo_g").mouseenter(function(){
    $(this).attr("src", "image/box-google2.png");
  });

  //滑鼠離開 google 圖片
  $("img.logo_g").mouseleave(function(){
    $(this).attr("src", "image/box-google1.png");
  });

  //滑鼠移到 facebook 圖片
  $("img.logo_f").mouseenter(function(){
    $(this).attr("src", "image/box-facebook2.png");
  });

  //滑鼠離開 facebook 圖片
  $("img.logo_f").mouseleave(function(){
    $(this).attr("src", "image/box-facebook1.png");
  });

  //滑鼠移到 line 圖片
  $("img.logo_l").mouseenter(function(){
    $(this).attr("src", "image/box-line2.png");
  });

  //滑鼠離開 line 圖片
  $("img.logo_l").mouseleave(function(){
    $(this).attr("src", "image/box-line1.png");
  });
})