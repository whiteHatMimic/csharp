<?php
    //取得目前檔案的名稱。透過$_SERVER['PHP_SELF']先取得路徑
    $current_file = $_SERVER['PHP_SELF'];
    //echo $current_file; //查看目前取得的檔案完整
    
    //然後透過 basename 取得檔案名稱，加上第二個參數".php"，主要是將取得的檔案去掉 .php 這副檔名稱
    $current_file = basename($current_file, '.php');

    $index = null;

    switch ($current_file)
    {
        case 'test2': #章節
            $index = 1;
            break;
        case 'test3': #網誌
            $index = 2;
            break;
        case 'tess': #免費軟體
            $index = 3;
            break;
        case 'Privacy_Policy': #隱私政策
            $index = 4;
            break;
        case 'cs_download': #下載
            $index = 5;
            break;
        default:
            $index = 0; #其它首頁
            break;
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="X-UA-Compatible" content="chrome=1" charset="UTF-8">
    <title>test1</title>
    <link rel="stylesheet" href="../css/reset.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <style>
        /*開頭標題*/
        #top
        {
            background-color:  #ffffff;
            padding: 10px;
        }

        #top span.CShool a
        {
            color: #5f5f5f;
            font-size: 40px;
            text-decoration: none;
        }

        #top span.com a
        {
            color: #00bfff;
            font-size: 40px;
            text-decoration: none;
        }

        #top span.content
        {
            float: right;
            margin-top: 35px;
        }

        #top span.CShool a:hover
        {
            color: #00bfff;
        }

        /*功能選單*/
        #menu
        {
            top: 0px;
            position:sticky;
            z-index: 999;
        }

        #menu ul
        {
            background-color: #5f5f5f;
        }

        #menu ul.level1 li.menu_color
        {
            background-color: #00bfff;
        }

        #menu ul.level1 a
        {
            display: block;
            text-decoration: none;
            font-size: 20px;
            color: #ffffff;
            padding: 15px;
        }

        #menu ul.level1 li
        {
            display: inline-block;
        }

        #menu ul.level1 li a:hover
        {
            background-color: #000000;
        }
    </style>
</head>
<body>
    <!--top-->
    <div id="top">
      <span>
        <span class="CShool">
          <a href="index.php"><b>C# School</b></a>
        </span>
        <span class="com">
          <a href="index.php"><b>.com</b></a>
        </span>
        <span class="content">
          程式帶給我們人類無限夢想
        </span>
      </span>
    </div>

<div id="menu">
  <ul class="level1">
    <li>
      <a class="list" href="javascript:void(0);">
        <i class="fas fa-list"></i>
      </a>
    </li>
    <li <?php echo ($index == 0)?"class='menu_color'":""?>><a href="test1.php">首頁</a></li>
    <li <?php echo ($index == 1)?"class='menu_color'":""?>><a href="test2.php">章節</a></li>
    <li <?php echo ($index == 2)?"class='menu_color'":""?>><a href="test3.php">網誌</a></li>
    <li <?php echo ($index == 3)?"class='menu_color":""?>><a href="free_Software.php">免費軟體</a></li>
    <li <?php echo ($index == 4)?"class='menu_color'":""?>><a href="Privacy_Policy.php">隱私政策</a></li>
    <li <?php echo ($index == 5)?"class='menu_color'":""?>><a href="cs_download.php">下載</a></li>
  </ul>
</div>
</body>
</html>