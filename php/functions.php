<?php
  @session_start();

  //取得所有章節內容
  function get_article_content($id)
  {
    $result = null;
    
    $id = filter_var($id, FILTER_VALIDATE_INT);

    $sql = "SELECT `id`, `title`, `content` FROM `chapter_list` WHERE `id` = '{$id}'";

    $query = mysqli_query($_SESSION['link'], $sql);

    if ($query)
    {
      if (mysqli_num_rows($query) == 1)
      {
        $result = mysqli_fetch_assoc($query);
      }

      mysqli_free_result($query);
    }
    else
    {
      echo '{$sql}語法請求失敗' . mysqli_error($_SESSION['link']);
    }

    return $result;
  }

  //取得所有章節標題
  function get_all_title()
  {
    $date = array();

    $sql = "SELECT `id`, `title` FROM `chapter_list` WHERE `publish` = '1'";

    $query = mysqli_query($_SESSION['link'], $sql);

    if ($query)
    {
      if (mysqli_num_rows($query) >= 0)
      {
        while($row = mysqli_fetch_assoc($query))
        {
          $date[] = $row;
        }
      }
    }
    else
    {
      echo '{$sql}語法請求失敗' . mysqli_error($_SESSION['link']);
    }

    return $date;
  }

  function get_all_blog()
  {
    $data = array();

    $sql = "SELECT `id`, `title`, `content`, `addDate` FROM `blog_list` WHERE `publish` = '1'";

    $query = mysqli_query($_SESSION['link'], $sql);

    if ($query)
    {
      if (mysqli_num_rows($query) > 0)
      {
        while ($row = mysqli_fetch_assoc($query))
        {
          $data[] = $row;
        }
      }

      mysqli_free_result($query);
    }
    else
    {
      echo '{$sql}語法請求失敗' . mysqli_error($_SESSION['link']);
    }

    return $data;
  }

  function get_blog_id($id)
  {
    $resutl = null;

    $id = filter_var($id, FILTER_SANITIZE_MAGIC_QUOTES);

    $sql = "SELECT `id`, `title`, `content`, `addDate` FROM `blog_list` WHERE `id` = '{$id}'";

    $query = mysqli_query($_SESSION['link'], $sql);

    if ($query)
    {
      if(mysqli_num_rows($query) == 1)
      {
        $result = mysqli_fetch_assoc($query);
      }
    }
    else
    {
      echo '{$sql}語法請求失敗' . mysqli_error($_SESSION['link']);
    }

    return $result;
  }
?>
