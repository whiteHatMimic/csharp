<?php
  //防止 httponly 存取
  ini_set("session.cookie_httponly", 1);

  //啟動 session 等下要來儲存 連線後的資訊，前面加上 @ 是為了要讓此方法執行時，若有錯誤訊息不要顯示。
  session_start();
  
  //連上資料庫的位置
  $server = 'localhost';
  //資料庫帳號名稱
  $server_name = 'root';
  //資料庫密碼
  $server_passwd = 'mimic980011309';
  //連上的資料庫名稱
  $server_DataBase = 'csharp';

  //宣告一個 link 變數，並執行連結資料庫函式 mysqli_connect()，連結結果會帶入 link 當中
  $_SESSION['link'] = mysqli_connect($server, $server_name, $server_passwd, $server_DataBase);

  if ($_SESSION['link'])
  {
    //若傳回正值，就代表已經連線
    //設定連線編碼為UTF-8
    //mysqli_query(資料庫連線, "語法內容") 為執行sql語法的函式
    mysqli_query($_SESSION['link'], "SET NAMES utf8");
    //echo '已正確連上資料庫';
  }
  else
  {
    //否則就代表連線失敗 mysqli_connect_error() 是顯示連線錯誤訊息
    echo '無法連線mysql資料庫 :<br/>' . mysqli_connect_error();
  }
?>
