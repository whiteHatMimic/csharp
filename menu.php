<?php
  //取得目前檔案的名稱。透過$_SERVER['PHP_SELF']先取得路徑
  $current_file = $_SERVER['PHP_SELF'];
  //echo $current_file; //查看目前取得的檔案完整
    
  //然後透過 basename 取得檔案名稱，加上第二個參數".php"，主要是將取得的檔案去掉 .php 這副檔名稱
  $current_file = basename($current_file, '.php');

  $index = null;

  switch ($current_file)
  {
    case 'chapter': #章節
      $index = 1;
      break;
    case 'blog': #網誌
    case 'blog_more':
      $index = 2;
      break;
    case 'Privacy_Policy': #隱私政策
      $index = 3;
      break;
    case 'support': #下載
      $index = 4;
      break;
    default:
      $index = 0; #其它...回首頁
      break;
  }
?>

<div id="menu">
  <ul class="level1">
    <li class="logo">
      <a href="javascript:void(0);">
        <i class="fas fa-list"></i>
      </a>
    </li>
    <li class="more"><a href="javascript:void(0);">more</a></li>
    <li <?php echo ($index == 0)?"class='main'":"class='list'"?>><a href="index.php">首頁</a></li>
    <li <?php echo ($index == 1)?"class='main'":"class='list'"?>><a href="./chapter.php?cha=<?php echo '1'?>">章節</a></li>
    <li <?php echo ($index == 2)?"class='main'":"class='list'"?>><a href="blog.php">網誌</a></li>
    <li <?php echo ($index == 3)?"class='main'":"class='list'"?>><a href="Privacy_Policy.php">隱私政策</a></li>
    <li <?php echo ($index == 4)?"class='main'":"class='list'"?>><a href="support.php">下載</a></li>
  </ul>
</div>