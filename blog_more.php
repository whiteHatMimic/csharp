<?php
  header("X-Frame-Options: DENY");

  require_once 'php/db.php';
  require_once 'php/functions.php';

  $get_blog = get_blog_id($_GET['blo']);

  if (is_null($get_blog))
  {
    header("Location: ./blog.php");
  }
?>
<!DOCTYPE html>
<html>
  <head>
    <title>C# School</title>
    <meta http-equiv="X-UA-Compatible" content="chrome=1" charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="image/logo.ico">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/cs_blog_more.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!--[if lt IE 7.]>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
    <![endif]-->
    <script src="jquery/switch_Picture.js"></script>
  </head>
  <body>
    <!--top-->
    <div id="top">
      <div class="row">
        <div class="cs-col sm12 md12 lg12 xl12">
          <div class="card text_center">
            <a class="CShool" href="index.php">C# School
              <span class="com">.com</span>
            </a>
          </div>
        </div>
      </div>
    </div>

    <!--menu-->
    <?php
      include_once 'menu.php';
    ?>

    <div id="article">
      <div class="container">
        <div class="row">
          <?php if (!empty($get_blog)):?>
            <?php
              $title = strip_tags($get_blog['title']);
            ?>

            <div class="cs-col sm12 md12 lg12 xl12">
              <div class="card">
                <h1 class="title"><?php echo $title;?></h1>
              </div>
            </div>

            <div class="cs-col sm12 md12 lg12 xl12">
              <div class="card">
                <p class="data"><span><i class="fas fa-clock"></i></span><?php echo $get_blog['addDate'];?></p>
              </div>
            </div>

            <div class="cs-col sm12 md12 lg12 xl12">
              <div class="card">
                <p class="content"><?php echo "{$get_blog['content']}"?></p>
              </div>
            </div>
          <?php else:?>
            <div class="cs-col sm12 md12 lg12 xl12">
              <p>無內容</p>
            </div>
          <?php endif;?>
        </div>
      </div>
    </div>

    <!--聯絡我們-->
    <div id="contact">
      <div class="container">
        <div class="row">
          <div class="cs-col sm12 md12 lg12 xl12">
            <div class="card">
              <h2 class="title">聯繫我們</h2>
            </div>
          </div>

          <div class="cs-col sm12 md12 lg12 xl12">
            <div class="card">
              <!--連結google社群-->
              <a href="https://plus.google.com/u/0/communities/109335508485514749844" target="_blank">
                <img class="logo_g" src="image/box-google1.png">
              </a>

              <!--連結facebook社群-->
              <a href="https://www.facebook.com/groups/534251500275740/" target="_blank">
                <img class="logo_f" src="image/box-facebook1.png">
              </a>

              <!--連結line社群-->
              <a href="http://line.me/ti/p/%40ino5143k" target="_blank">
                <img class="logo_l" src="image/box-line1.png">
              </a>
            </div>
          </div>

          <div class="cs-col sm12 md12 lg12 xl12">
            <div class="card">
              <p class="copyright">Copyright &copy; <?php echo date("Y")?> C#-School. All rights reserved</p>
            </div>
          </div>
        </div><!--row-->
      </div><!--container-->
    </div><!--contact-->
  </body>
</html>

<script>
  $(document).on("ready", function(){
    $(window).on("resize", function(){
      var w = $(window).width();

      if (w > 768)
      {
        $("#menu ul.level1 li.list").removeAttr('style');
        $("#menu ul.level1 li.main").removeAttr('style');
      }
    })

    $("#menu ul.level1 li.logo").on("click", function(){
        $("#menu ul.level1 li.list").toggle();
        $("#menu ul.level1 li.main").toggle();
        $("#menu ul.level1 li.list").css({"z-index": "999"});
    })
  })
</script>