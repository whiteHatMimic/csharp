<?php
  header("X-Frame-Options: DENY");

  require_once 'php/db.php';
  require_once 'php/functions.php';

  $dates = get_all_blog();
?>
<!DOCTYPE html>
<html>
  <head>
    <title>C# School</title>
    <meta http-equiv="X-UA-Compatible" content="chrome=1" charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="image/logo.ico">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/cs_blog.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!--[if lt IE 7.]>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
    <![endif]-->
    <script src="jquery/switch_Picture.js"></script>
  </head>
  <body>
    <!--top-->
    <div id="top">
      <div class="row">
        <div class="cs-col sm12 md12 lg12 xl12">
          <div class="card text_center">
            <a class="CShool" href="index.php">C# School
              <span class="com">.com</span>
            </a>
          </div>
        </div>
      </div>
    </div>

    <!--menu-->
    <?php
      include_once 'menu.php';
    ?>

    <div id="space">
      <div class="container">
        <div class="row">
          <?php if (!empty($dates)):?>
            <?php foreach($dates as $key=>$row):?>
              <?php
                $title = strip_tags($row['title']);
                $content = strip_tags($row['content']);
                $content = mb_substr($content, 0, 100, "UTF-8");
              ?>

              <div class="cs-col sm12 md6 lg4 xl4">
                <div class="card">
                  <h2 class="title"><?php echo $title;?></h2>
                  <p class="date"><span><i class="fas fa-clock"></i></span><?php echo $row['addDate'];?></p>
                  <p class="content"><?php echo $content?></p>
                  <a class="more" href="blog_more.php?blo=<?php echo $row['id'];?>">more></a>
                </div>
              </div>
            <?php endforeach;?>
          <?php else:?>
            <p>無內容</p>
          <?php endif;?>
        </div>
      </div>
    </div>

    <!--聯絡我們-->
    <div id="contact">
      <div class="container">
        <div class="row">
          <div class="cs-col sm12 md12 lg12 xl12">
            <div class="card">
              <h2 class="title">聯繫我們</h2>
            </div>
          </div>

          <div class="cs-col sm12 md12 lg12 xl12">
            <div class="card">
              <!--連結google社群-->
              <a href="https://plus.google.com/u/0/communities/109335508485514749844" target="_blank">
                <img class="logo_g" src="image/box-google1.png">
              </a>

              <!--連結facebook社群-->
              <a href="https://www.facebook.com/groups/534251500275740/" target="_blank">
                <img class="logo_f" src="image/box-facebook1.png">
              </a>

              <!--連結line社群-->
              <a href="http://line.me/ti/p/%40ino5143k" target="_blank">
                <img class="logo_l" src="image/box-line1.png">
              </a>
            </div>
          </div>

          <div class="cs-col sm12 md12 lg12 xl12">
            <div class="card">
              <p class="copyright">Copyright &copy; <?php echo date("Y")?> C#-School. All rights reserved</p>
            </div>
          </div>
        </div><!--row-->
      </div><!--container-->
    </div><!--contact-->
  </body>
</html>

<script>
  $(document).on("ready", function(){
    $(window).on("resize", function(){
      var w = $(window).width();

      if (w > 768)
      {
        $("#menu ul.level1 li.list").removeAttr('style');
        $("#menu ul.level1 li.main").removeAttr('style');
      }
    })

    $("#menu ul.level1 li.logo").on("click", function(){
        $("#menu ul.level1 li.list").toggle();
        $("#menu ul.level1 li.main").toggle();
        $("#menu ul.level1 li.list").css({"z-index": "999"});
    })
  })
</script>
