<?php
  header("X-Frame-Options: DENY");
?>

<!DOCTYPE html>
<html>
  <head>
    <title>C# School</title>
    <meta http-equiv="X-UA-Compatible" content="chrome=1" charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="image/logo.ico">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/menu.css">
    <link rel="stylesheet" href="css/cs_support.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!--[if lt IE 7.]>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
    <![endif]-->
    <script src="jquery/switch_Picture.js"></script>
  </head>
  <body>
    <!--top-->
    <div id="top">
      <div class="row">
        <div class="cs-col sm12 md12 lg12 xl12">
          <div class="card text_center">
            <a class="CShool" href="index.php">C# School
              <span class="com">.com</span>
            </a>
          </div>
        </div>
      </div>
    </div>

    <!--menu-->
    <?php
      include_once 'menu.php';
    ?>

    <div id="vs_2017">
      <div class="container">
        <div class="row">
          <div class="cs-col sm12 md12 lg12 xl12">
            <h1 class="title">Visual Studio 2017 下載</h1>
            <p class="content">功能完善的整合式開發環境 (IDE)，適用於 Android、iOS、Windows、Web 及雲端</p>
          </div>

          <div class="cs-col sm12 md12 lg4 xl4">
            <div class="card">
              <h2 class="title">Visual Studio Community 2017</h2>
              <p class="content">是微軟面費提供給學生、開放原始碼三與者及個人開 發人員使用</p>
              <p class="support">
                系統支援: 
                <i class="fab fa-windows"></i>
                <i class="fab fa-apple"></i>
              </p>
              <p class="official">官方下載: <a class="link" href="http://www.visualstudio.com/zh-hant/vs/whatsnew" target="_blank">http://www.visualstudio.com/zh-hant/vs/whatsnew</a></p>
              <a class="download" href="downloads/vs_2017/vs_community_2017.exe">點擊下載<i class="fas fa-download"></i></a>
            </div>
          </div>

          <div class="cs-col sm12 md12 lg4 xl4">
            <div class="card">
              <h2 class="title">Visual Studio Professional 2017</h2>
              <p class="content">適用於小型團隊的專業開發人員，想開發出非指向性重要應用的團體專業開發者</p>
              <p class="support">
                系統支援: 
                <i class="fab fa-windows"></i>
                <i class="fab fa-apple"></i></p>
              <p class="official">官方下載: <a class="link" href="http://www.visualstudio.com/zh-hant/vs/whatsnew" target="_blank">http://www.visualstudio.com/zh-hant/vs/whatsnew</a></p>
              <a class="download"  href="downloads/vs_2017/vs_professional_2017.exe">點擊下載<i class="fas fa-download"></i></a>
            </div>
          </div>

          <div class="cs-col sm12 md12 lg4 xl4">
            <div class="card">
              <h2 class="title">Visual Studio Enterprise 2017</h2>
              <p class="content">滿足各規模團隊對嚴格品質與規模需求的端對端解決方案</p>
              <p class="support">
                系統支援: 
                <i class="fab fa-windows"></i>
                <i class="fab fa-apple"></i></p>
              <p class="official">官方下載: <a class="link" href="http://www.visualstudio.com/zh-hant/vs/whatsnew" target="_blank">http://www.visualstudio.com/zh-hant/vs/whatsnew</a></p>
              <a class="download"  href="downloads/vs_2017/vs_enterprise_2017.exe">點擊下載<i class="fas fa-download"></i></a>
            </div>
          </div>
        </div><!--row-->
      </div><!--container-->
    </div>
    
    <!-- vs_13_15 -->
    <div id="vs_13_15">
      <div class="container">
        <div class="row">
          <div class="cs-col sm12 md12 lg12 xl12">
            <div class="card">
              <h1 class="title">其他版本選擇</h1>
            </div>
          </div>

          <!-- vs_2015 -->
          <div class="cs-col sm12 md12 lg4 xl4">
            <div class="card">
              <a class="vs_2015" href="downloads/vs_2015/vs_community_2015.exe">Visaul Studio Community 2015</a>
            </div>
          </div>

          <div class="cs-col sm12 md12 lg4 xl4">
            <div class="card">
              <a class="vs_2015" href="downloads/vs_2015/vs_professional_2015.exe">Visaul Studio Professional 2015</a>
            </div>
          </div>

          <div class="cs-col sm12 md12 lg4 xl4">
            <div class="card">
              <a class="vs_2015" href="downloads/vs_2015/vs_enterprise_2015.exe">Visaul Studio Enterprise 2015</a>
            </div>
          </div>

          <!-- vs_2013 -->
          <div class="cs-col sm12 md12 lg4 xl4">
            <div class="card">
              <a class="vs_2013" href="downloads/vs_2013/vs_community_2013.exe">Visaul Studio Community 2013</a>
            </div>
          </div>

          <div class="cs-col sm12 md12 lg4 xl4">
            <div class="card">
              <a class="vs_2013" href="downloads/vs_2013/">Visaul Studio Professional 2013</a>
            </div>
          </div>

          <div class="cs-col sm12 md12 lg4 xl4">
            <div class="card">
              <a class="vs_2013" href="downloads/vs_2013/">Visaul Studio Enterprise 2013</a>
            </div>
          </div>
        </div><!--row-->
      </div><!--contaienr-->
    </div>

    <!--聯絡我們-->
    <div id="contact">
      <div class="container">
        <div class="row">
          <div class="cs-col sm12 md12 lg12 xl12">
            <div class="card">
              <h2 class="title">聯繫我們</h2>
            </div>
          </div>

          <div class="cs-col sm12 md12 lg12 xl12">
            <div class="card">
              <!--連結google社群-->
              <a href="https://plus.google.com/u/0/communities/109335508485514749844" target="_blank">
                <img class="logo_g" src="image/box-google1.png">
              </a>

              <!--連結facebook社群-->
              <a href="https://www.facebook.com/groups/534251500275740/" target="_blank">
                <img class="logo_f" src="image/box-facebook1.png">
              </a>

              <!--連結line社群-->
              <a href="http://line.me/ti/p/%40ino5143k" target="_blank">
                <img class="logo_l" src="image/box-line1.png">
              </a>
            </div>
          </div>

          <div class="cs-col sm12 md12 lg12 xl12">
            <div class="card">
              <p class="copyright">Copyright &copy; <?php echo date("Y")?> C#-School. All rights reserved</p>
            </div>
          </div>
        </div><!--row-->
      </div><!--container-->
    </div><!--contact-->
  </body>
</html>

<script>
  $(document).on("ready", function(){
    $(window).on("resize", function(){
      var w = $(window).width();

      if (w > 768)
      {
        $("#menu ul.level1 li.list").removeAttr('style');
        $("#menu ul.level1 li.main").removeAttr('style');
      }
    })

    $("#menu ul.level1 li.logo").on("click", function(){
        $("#menu ul.level1 li.list").toggle();
        $("#menu ul.level1 li.main").toggle();
        $("#menu ul.level1 li.list").css({"z-index": "999"});
    })
  })
</script>