<?php
  require_once 'php/db.php';
  require_once 'php/functions.php';

  //取得所有會員資料
  $dates = get_all_users();

  if (!isset($_SESSION['is_login']) || !$_SESSION['is_login'])
  {
    header("Location: login.php");
  }
?>

<!DOCTYPE html>
<html>
  <head>
    <title>C# School</title>
    <meta http-equiv="X-UA-Compatible" content="chrome=1" charset="utf-8">
    <link rel="shortcut icon" href="image/logo.ico">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/menu.css">
    <link rel="stylesheet" href="css/member.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!--[if lt IE 7.]>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
    <![endif]-->
    <script src="jquery/switch_Picture.js"></script>
    <script src="jquery/del_member.js"></script>
  </head>
  <body>
    <div id="uname">
      <span class="username"><b><?php echo $_SESSION['is_username'];?></b></span>
      <span class="Mpasswd"><b><a href="./modify_user.php">修改密碼</a></b></span>
      <span class="loginOut"><b><a href="php/loginOut.php">登出</a></b></span>
    </div>

    <!--top-->
    <div id="top">
      <span>
        <span class="CShool">
          <a href="index.php"><b>C# School</b></a>
        </span>
        <span class="com">
          <a href="index.php"><b>.com</b></a>
        </span>
        <span>
          <span class="bottom"><b>後台</b></span>
        </span>
      </span>
    </div>

    <!--menu-->
    <?php
      include_once 'menu.php';
    ?>

    <div id="container1">
      <div class="con-scope">
        <!-- 功能選項 -->
        <div class="box-tool">
          <div class="con-ac">
            <div class="navbar">
              <div class="dropdown">
                <a class="add-un" href="./memberadd.php">新增帳號</a>
              </div>
            </div>
          </div>
        </div>

        <!-- contents -->
        <div class="user-content">
          <div class="content">
            <table>
              <tr class="box-black">
                <td></td>
                <td>使用者名稱</td>
                <td>帳號</td>
                <td>角色</td>
                <td>狀態</td>
                <td>管理動作</td>
              </tr>
              <?php if(!empty($dates)):?>
                <?php foreach($dates as $key=>$row):?>
                  <tr class="box-gray" <?php echo ($key % 2 != 0)?"style='background-color:#d9d9d9'":"style='background-color:#ffffff'"?>>
                    <th class="usernumber"><?php echo $key + 1;?></th>
                    <th><?php echo $row['username'];?></th>
                    <th><?php echo $row['account'];?></th>
                    <th><?php echo ($row['identity'] == 'A')?"管理者":"會員"; ?></th>
                    <th><img src="image/circle.png" style="width: 15px; "><?php echo ($row['state'] == '1')?"正常":"停用"?></th>
                    <th>
                      <?php if ($row['username'] == $_SESSION['is_username']):?>
                        <a class="edit" href='memberedit.php?mem=<?php echo $row['id'];?>'>編輯</a>
                      <?php else:?>
                        <a class="edit" href='memberedit.php?mem=<?php echo $row['id'];?>'>編輯</a>
                        <a class="delete" href="javascript:void(0);" data-id="<?php echo $row ['id'];?>">刪除</a>
                      <?php endif;?>
                    </th>
                  </tr>
                <?php endforeach;?>
              <?php else:?>
                <tr>
                  <th colspan="6">無資料</th>
                </tr>
              <?php endif;?>
            </table>
          </div>
        </div>
      </div>
    </div>

    <!--聯絡方式-->
    <div id="contact">
      <h2 class="title">聯繫我們</h2>

      <!--連結google社群-->
      <span>
        <a href="https://plus.google.com/u/0/communities/109335508485514749844" target="_blank">
          <img class="box-g" src="image/box-google1.png">
        </a>
      </span>

      <!--連結facebook社群-->
      <span>
        <a href="https://www.facebook.com/groups/534251500275740/" target="_blank">
          <img class="box-f" src="image/box-facebook1.png">
        </a>
      </span>

      <!--連結line社群-->
      <span>
        <a href="http://line.me/ti/p/%40ino5143k" target="_blank">
          <img class="box-l" src="image/box-line1.png">
        </a>
      </span>

      <span>
        <p>Copyright &copy; <?php echo date("Y")?> C#-School. All rights reserved</p>
      </span>
    </div>
  </body>
</html>
