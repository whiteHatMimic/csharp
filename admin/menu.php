<?php
  //取得目前檔案的名稱。透過$_SERVER['PHP_SELF']先取得路徑
  $current_file = $_SERVER['PHP_SELF'];
  //然後透過 basename 取得檔案名稱，加上第二個參數".php"，主要是將取得的檔案去掉 .php 這副檔名稱
  $current_file = basename($current_file, '.php');

  $index = 0;

  switch ($current_file)
  {
    case 'index':
    case 'chapteredit':
    case 'chapteradd':
      $index = 0;
      break;
    case 'blog':
    case 'blogadd':
    case 'blogedit':
      $index = 1;
      break;
    case 'member':
    case 'memberedit':
    case 'memberadd':
      $index = 2;
      break;
  }
?>

<?php if (isset($_SESSION['is_identity']) == 'A' && $_SESSION['is_identity'] == 'A'):?>
  <div id="menu">
    <ul class="level1">
      <li <?php echo ($index == 0)?"class='menu_color'":""?>><a href="index.php"><b>章節管理</b></a></li>
      <li <?php echo ($index == 1)?"class='menu_color'":""?>><a href="blog.php"><b>網誌管理</b></a></li>
      <li <?php echo ($index == 2)?"class='menu_color'":""?>><a href="member.php"><b>會員管理</b></a></li>
    </ul>
  </div>
<?php else: ?>
  <div id="menu">
    <ul class="level1">
      <li <?php echo ($index == 0)?"class='menu_color'":""?>><a href="index.php"><b>章節管理</b></a></li>
      <li <?php echo ($index == 1)?"class='menu_color'":""?>><a href="blog.php"><b>網誌管理</b></a></li>
    </ul>
  </div>
<?php endif;?>
