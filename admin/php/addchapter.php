<?php
    require_once 'db.php';
    require_once 'functions.php';
    
    if (isset($_SESSION['is_login']) && $_SESSION['is_login'])
    {
        $addchapter = addchapter($_POST['title'], $_POST['content'], $_POST['publish'], $_POST['username']);

        if ($addchapter)
        {
            echo 'yes';
        }
        else
        {
            echo 'no';
        }
    }
    else
    {
        echo 'no';
    }
?>