<?php
    @session_start();

    //驗證使用者帳號密碼是否正確
    function verify_Account_and_Passwd($account, $password)
    {
        $result = null;
        $pw = md5($password);
        $id = 'A';
        $sql = "SELECT * FROM `sgin_up` WHERE `account` = '{$account}' AND `password` = '{$pw}' AND `state` = '1'";

        $query = mysqli_query($_SESSION['link'], $sql);

        if ($query)
        {
            //使用 mysqli_num_rows 方法，判別執行的語法，其取得的資料量，是否有一筆資料
            if (mysqli_num_rows($query) == 1)
            {
                //取得使用者資料
                $user = mysqli_fetch_assoc($query);

                //在session裡設定 is_login 並給 true 值，代表已經登入
                $_SESSION['is_login'] = true;

                //在session裡設定 is_username 並給 目前登入帳號名稱
                $_SESSION['is_username'] = $user['username'];

                //在session裡設定 is_account 並給 目前登入帳號
                $_SESSION['is_account'] = $user['account'];

                //在session裡設定 is_account 並給 目前登入身分
                $_SESSION['is_identity'] = $user['identity'];

                //回傳的 $result 就給 true 代表有該帳號，不可以被新增
                $result = true;
            }
        }
        else
        {
            echo '{$sql}語法請求失敗<br>' . mysqli_error($_SESSION['link']);
        }

        //回傳值回去給 check_username.php 判斷名稱有沒有重複
        return $result;
    }

    //先驗證帳號密碼無誤之後在呼叫 modify_pw 方法修改密碼
    function verify_user_data($account, $password)
    {
        $result = null;

        $password = md5($password);

        $sql = "SELECT `account`, `password` FROM `sgin_up` WHERE `account` = '{$account}' AND `password` = '{$password}';";

        $query = mysqli_query($_SESSION['link'], $sql);

        if ($query)
        {
            if (mysqli_num_rows($query) == 1)
            {
                $result = true;
            }
        }
        else
        {
            echo '{$sql1}語法請求失敗<br>' . mysqli_error($_SESSION['link']);
        }

        return $result;
    }

    //修改使用者密碼
    function modify_pw($account, $newpassword)
    {
        $result = null;

        $newpassword = md5($newpassword);

        $sql = "UPDATE `sgin_up` SET `password` = '{$newpassword}' WHERE `account` = '{$account}';";

        $query = mysqli_query($_SESSION['link'], $sql);

        if ($query)
        {
            if (mysqli_affected_rows($_SESSION['link']) == 1)
            {
                $result = true;
            }
        }
        else
        {
            echo '{$sql}語法請求失敗<br>' . mysqli_error($_SESSION['link']);
        }

        return $result;
    }

    //取得所有文章資料
    function get_all_chapter()
    {
        $dates = array();

        $sql = "SELECT * FROM `chapter_list`";

        $query = mysqli_query($_SESSION['link'], $sql);

        if ($query)
        {
            if (mysqli_num_rows($query) > 0)
            {
                while ($row = mysqli_fetch_assoc($query))
                {
                    $dates[] = $row;
                }
            }

            mysqli_free_result($query);
        }
        else
        {
            echo '{$sql}語法請求失敗<br>' . mysqli_error($_SESSION['link']);
        }

        return $dates;
    }

    //新增課程文章
    function addchapter($title, $content, $publish, $username)
    {
        $result = null;

        $nowTime = date('Y-m-d H:i:s');

        $sql = "INSERT INTO `chapter_list` (`title`, `content`, `build_Staff`,
         `last_modified_Staff`, `publish`, `addDate`, `Mdate`) VALUES ('{$title}',
          '{$content}', '{$username}', '{$username}', '{$publish}', CURRENT_TIMESTAMP,
           '{$nowTime}');";

        $query = mysqli_query($_SESSION['link'], $sql);

        if ($query)
        {
            if (mysqli_affected_rows($_SESSION['link']) == 1)
            {
                $result = true;
            }
        }
        else
        {
            echo '{$sql}語法請求失敗' . mysqli_error($_SESSION['link']);
        }
        
        return $result;
    }

    //更新課程文章
    function update_chapter($id, $title, $content, $publish, $username)
    {
        $result = null;

        $nowTime = date("Y-m-d H:i:s");

        $sql = "UPDATE `chapter_list` SET `title` = '{$title}', `content` = '{$content}', `publish` = '{$publish}', `last_modified_Staff`= '{$username}', `Mdate`='{$nowTime}' WHERE `id`= '{$id}'";

        $query = mysqli_query($_SESSION['link'], $sql);

        if ($query)
        {
            if (mysqli_affected_rows($_SESSION['link']) == 1)
            {
                $result = true;
            }
        }
        else
        {
            echo '{$sql}語法請求失敗' . mysqli_error($_SESSION['link']);
        }

        return $result;
    }

    //刪除文章資料
    function del_chapter($id)
    {
        $result = null;

        $sql = "DELETE FROM `chapter_list` WHERE `id` = '{$id}'";

        $query = mysqli_query($_SESSION['link'], $sql);

        if ($query)
        {
            if (mysqli_affected_rows($_SESSION['link']) == 1)
            {
                $result = true;
            }
        }
        else
        {
            echo '{$sql}語法請求失敗' . mysqli_error($_SESSION['link']);
        }

        return $result;
    }

    //撈取文章 id 資料
    function get_chapter_id($id)
    {
        $result = null;

        $id = htmlentities($id, ENT_QUOTES, 'UTF-8');

        $sql = "SELECT * FROM `chapter_list` WHERE `id` = '{$id}';";

        $query = mysqli_query($_SESSION['link'], $sql);

        if ($query)
        {
            if (mysqli_num_rows($query) == 1)
            {
                $result = mysqli_fetch_assoc($query);
            }

            mysqli_free_result($query);
        }
        else
        {
            echo '{$sql}語法請求失敗<br>' . mysqli_error($_SESSION['link']);
        }

        return $result;
    }

    //取得所有使用者資料
    function get_all_users()
    {
        $dates = array();

        $sql = "SELECT `id`, `username`, `account`, `identity`, `state` FROM `sgin_up` ORDER BY `identity` ASC;";

        $query = mysqli_query($_SESSION['link'], $sql);

        if ($query)
        {
            if (mysqli_num_rows($query) > 0)
            {
                while ($row = mysqli_fetch_assoc($query))
                {
                    $dates[] = $row;
                }
            }
        }
        else
        {
            echo '{$sql}語法請求失敗<br>' . mysqli_error($_SESSION['link']);
        }

        return $dates;
    }

    //取得使用者目前選取的
    function get_user_id($id)
    {
        $result = null;

        $id = htmlentities($id, ENT_QUOTES, 'UTF-8');

        $sql = "SELECT * FROM `sgin_up` WHERE `id` = '{$id}';";

        $query = mysqli_query($_SESSION['link'], $sql);

        if ($query)
        {
            if (mysqli_num_rows($query) == 1)
            {
                $result = mysqli_fetch_assoc($query);
            }

            mysqli_free_result($query);
        }
        else
        {
            echo '{$sql}語法請求失敗<br>' . mysqli_error($_SESSION['link']);
        }

        return $result;
    }

    //刪除使用者資料
    function del_user($id)
    {
        $result = null;

        $sql = "DELETE FROM `sgin_up` WHERE `id` = '{$id}'";

        $query = mysqli_query($_SESSION['link'], $sql);

        if ($query)
        {
            if (mysqli_affected_rows($_SESSION['link']) == 1)
            {
                $result = true;
            }
        }
        else
        {
            echo '{$sql}語法請求失敗<br>' . mysqli_error($_SESSION['link']);
        }

        return $result;
    }

    //檢查有沒有重複名稱
    function check_username($username)
    {
        $result = null;

        $sql = "SELECT * FROM `sgin_up` WHERE `username` = '{$username}';";

        $query = mysqli_query($_SESSION['link'], $sql);

        if ($query)
        {
        //使用 mysqli_num_rows 方法，判別執行的語法，其取得的資料量，是否有一筆資料
            if (mysqli_num_rows($query) == 1)
            {
                //取得的量大於0代表有資料
                //回傳的 $result 就給 true 代表有該帳號，不可以被新增
                $result = true;
            }

            //釋放資料庫查詢到的記憶體
            mysqli_free_result($query);
        }
        else
        {
            echo '{$sql}語法請求失敗<br>' . mysqli_error($_SESSION['link']);
        }

        //回傳值回去給 check_username.php 判斷名稱有沒有重複
        return $result;
    }

    //檢查有沒有重複的 account 帳號
    function check_account($account)
    {
        $result = null;

        $sql = "SELECT * FROM `sgin_up` WHERE `account` = '{$account}';";

        $query = mysqli_query($_SESSION['link'], $sql);

        if ($query)
        {
            //使用 mysqli_num_rows 方法，判別執行的語法，其取得的資料量，是否有一筆資料
            if (mysqli_num_rows($query) == 1)
            {
                //取得的量大於0代表有資料
                //回傳的 $result 就給 true 代表有該帳號，不可以被新增
                $result = true;
            }

            //釋放資料庫查詢到的記憶體
            mysqli_free_result($query);
        }
        else
        {
            echo '{$sql}語法請求失敗<br>' . mysqli_error($_SESSION['link']);
        }

        //回傳值回去給 check_username.php 判斷名稱有沒有重複
        return $result;
    }

    //更新會員資料
    function update_user($id, $username, $account, $identity, $state)
    {
        $result = null;

        $last_modified_Staff = $_SESSION['is_username']; //最後更新人員

        $Mdate = date("Y-m-d H:i:s");

        $sql = "UPDATE `sgin_up` SET `username` = '{$username}', `account` = '{$account}', `identity` = '{$identity}',
                `state` = '{$state}', `last_modified_Staff` = '{$last_modified_Staff}', `Mdate` = '{$Mdate}' WHERE `id` = '{$id}';";

        $query = mysqli_query($_SESSION['link'], $sql);

        if ($query)
        {
            if (mysqli_affected_rows($_SESSION['link']) == 1)
            {
                $result = true;
            }
        }
        else
        {
            echo '{$sql}語法請求失敗<br>' . mysqli_error($_SESSION['link']);
        }

        return $result;
    }

    //後台新增會員
    function add_member($username, $account, $password, $identity, $state)
    {
        $result = null;
        $pw = md5($password);
        $newDate = date("Y/m/d H:i:s");
        $newUsername = $_SESSION['is_username'];

        $sql = "INSERT INTO `sgin_up` (`username`, `account`, `password`, `identity`, `state`, `build_Staff`, `last_modified_Staff`, `AddDate`, `Mdate`)
                VALUES ('{$username}', '{$account}', '{$pw}', '{$identity}', '{$state}', '{$newUsername}', '{$newUsername}', CURRENT_TIMESTAMP, '{$newDate}')";

        $query = mysqli_query($_SESSION['link'], $sql);

        if ($query)
        {
            if (mysqli_affected_rows($_SESSION['link']) == 1)
            {
                $result = true;
            }
        }
        else
        {
            echo '{$sql}執行語法失敗，錯誤訊息請看: ' . mysqli_error($_SESSION['link']);
        }

        return $result;
    }

    //取得所有網誌
    function get_all_blog()
    {
        $date = array();

        $sql = "SELECT * FROM `blog_list`";

        $query = mysqli_query($_SESSION['link'], $sql);

        if ($query)
        {
            if (mysqli_num_rows($query) > 0)
            {
                while ($row = mysqli_fetch_assoc($query))
                {
                    $date[] = $row;
                }
            }

            mysqli_free_result($query);
        }
        else
        {
            echo '{$sql}語法請求失敗' . mysqli_error($_SESSION['link']);
        }

        return $date;
    }

    //新增文章
    function addblog($title, $content, $publish, $username)
    {
        $result = null;
        
        $nowTime = date('Y-m-d H:i:s');

        $sql = "INSERT INTO `blog_list` (`title`, `content`, `build_Staff`, `last_modified_Staff`, `publish`, `addDate`, `Mdate`) 
                VALUES ('{$title}', '{$content}', '{$username}', '{$username}', '{$publish}', CURRENT_TIMESTAMP, '{$nowTime}')";

        $query = mysqli_query($_SESSION['link'], $sql);

        if($query)
        {
            if (mysqli_affected_rows($_SESSION['link']) == 1)
            {
                $result = true;
            }
        }
        else
        {
            echo '{$sql} 語法失敗' . mysqli_error($_SESSION['link']);
        }

        return $result;
    }

    //更新網誌
    function update_blog($id, $title, $content, $publish, $username)
    {
        $newDate = date("Y-m-d H:i:s");

        $result = null;
        
        $sql = "UPDATE `blog_list` SET `title` = '{$title}', `content` = '{$content}', `last_modified_Staff` = '{$username}', `publish` = '{$publish}', `Mdate` = '{$newDate}' WHERE `id` = '{$id}'";

        $query = mysqli_query($_SESSION['link'], $sql);

        if ($query)
        {
            if (mysqli_affected_rows($_SESSION['link']) == 1)
            {
                $result = true;
            }
        }
        else
        {
            echo '{$sql} 語法失敗' . mysqli_error($_SESSION['link']);
        }

        return $result;
    }

    //刪除網誌
    function del_blog($id)
    {
        $result = null;

        $sql = "DELETE FROM `blog_list` WHERE `id` = '{$id}'";

        $query = mysqli_query($_SESSION['link'], $sql);

        if ($query)
        {
            if (mysqli_affected_rows($_SESSION['link']) == 1)
            {
                $result = true;
            }
        }
        else
        {
            echo '{$sql}語法請求失敗' . mysqli_error($_SESSION['link']);
        }

        return $result;
    }

    //取得網誌 id
    function get_blog_id($id)
    {
        $result = null;

        $id = htmlentities($id, ENT_QUOTES, 'UTF-8');

        $sql = "SELECT * FROM `blog_list` WHERE `id` = '{$id}'";

        $query = mysqli_query($_SESSION['link'], $sql);

        if ($query)
        {
            if (mysqli_num_rows($query) == 1)
            {
                $result = mysqli_fetch_assoc($query);
            }
        }
        else
        {
            echo '{$sql}語法請求失敗' . mysqli_error($_SESSION['link']);
        }

        return $result;
    }
?>