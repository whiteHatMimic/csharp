<?php
  require_once 'db.php';
  require_once 'functions.php';

  $verify_user_data = verify_user_data($_POST['ac'], $_POST['oldpw']);


  if (isset($_SESSION['is_login']) && $_SESSION['is_login'])
  {
    //如果 user 驗證資料無誤
    if ($verify_user_data)
    {
      //執行 modify_pw 的方法修改密碼
      $modify_pw = modify_pw($_POST['ac'], $_POST['newpw']);

      if ($modify_pw)
      {
        //密碼修改成功
        echo 'yes';
      }
      else
      {
        //密碼修改失敗
        echo 'no';
      }
    }
    else
    {
      //驗證 user 資料失敗
      echo 'no';
    }
  }
  else
  {
    echo 'no';
  }
?>
