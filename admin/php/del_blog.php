<?php
  require_once 'db.php';
  require_once 'functions.php';

  if (isset($_SESSION['is_login']) && $_SESSION['is_login'])
  {
    $del_blog = del_blog($_POST['id']);

    if ($del_blog)
    {
      echo 'yes';
    }
    else
    {
      echo 'no';
    }
  }
  else
  {
    echo 'no';
  }
?>
