<?php
    require_once 'db.php';
    require_once 'functions.php';

    if (isset($_SESSION['is_login']) && $_SESSION['is_login'])
    {
        $update_blog = update_blog($_POST['id'], $_POST['title'], $_POST['content'], $_POST['publish'], $_POST['username']);

        if ($update_blog == true)
        {
            echo 'yes';
        }
        else
        {
            echo 'no';
        }
    }
    else
    {
        echo 'no';
    }
?>