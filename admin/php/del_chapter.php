<?php
  require_once 'db.php';
  require_once 'functions.php';

  if (isset($_SESSION['is_login']) && $_SESSION['is_login'])
  {
    $del_chapter = del_chapter($_POST['id']);

    if ($del_chapter)
    {
      echo 'yes';
    }
    else
    {
      echo 'no';
    }
  }
  else
  {
    echo 'no';
  }
?>
