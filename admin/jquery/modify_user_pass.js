$(document).on("ready", function(){
    //帳號輸入特殊字時過濾文字
    $("#box-account").on("input", function(){
      this.value = this.value.replace(/[^0-9A-Za-z]/g, "");
    });
    
    //舊密碼輸入特殊字時過濾文字
    $("#box-oldpassword").on("input", function(){
      this.value = this.value.replace(/[^0-9A-Za-z]/g, "");
    });
    
    //新密碼輸入特殊字時過濾文字
    $("#box-newpassword").on("input", function(){
      this.value = this.value.replace(/[^0-9A-Za-z]/g, "");
    });
    
    //確認密碼輸入特殊字時過濾文字
    $("#box-confirmpassword").on("input", function(){
      this.value = this.value.replace(/[^0-9A-Za-z]/g, "");
    })
    
    //點擊帳號輸入框，將框線變為藍色
    $("#box-account").on("click", function(){
      $("#box-account").css({"border": "1px solid #00bfff"});
      $("#ex1").text("");
    })

    //點擊舊密碼輸入框，將框線變為藍色
    $("#box-oldpassword").on("click", function(){
      $("#box-oldpassword").css({"border": "1px solid #00bfff"});
      $("#ex2").text("");
    })

    //點擊新密碼輸入框，將框線變為藍色
    $("#box-newpassword").on("click", function(){
      $("#box-newpassword").css({"border": "1px solid #00bfff"});
      $("#ex3").text("");
    })
    
    //點擊確認密碼輸入框，將框線變為藍色
    $("#box-confirmpassword").on("click", function(){
      $("#box-confirmPassword").css({"border": "1px solid #00bfff"});
      $("#ex4").text("");
    })

    $("#box-submit").on("click", function(){
      var verify_account = false, verify_oldpasswd = false, verify_newpasswd = false, verify_confirmpassword = false;
      //如果舊密碼輸入為空
      if ($("#box-account").val() == '')
      {
        $("#box-account").css({"border": "1px solid #ff0000"}); //將文字框顏色設定程紅色
        $("#ex1").text("未輸入帳號").css({"color": "#ff0000"}); //提醒未輸入帳號
      }
      else
      {
        verify_account = true;
      }

      //如果舊密碼輸入為空
      if ($("#box-oldpassword").val() == '')
      {
        $("#box-oldpassword").css({"border": "1px solid #ff0000"}); //將文字框顏色設定程紅色
        $("#ex2").text("未輸入密碼").css({"color": "#ff0000"}); //提醒未輸入密碼
      }
      else
      {
        verify_oldpasswd = true;
      }

      //如果新密碼輸入為空
      if ($("#box-newpassword").val() == '')
      {
        $("#box-newpassword").css({"border": "1px solid #ff0000"}); //將文字框顏色設定程紅色
        $("#ex3").text("未輸入密碼").css({"color": "#ff0000"}); //提醒未輸入密碼
      }
      else if ($("#box-newpassword").val().length < 8) //如果新的密碼少於 8 碼
      {
        $("#box-oldpassword").val('');
        $("#box-newpassword").val('');
        $("#box-confirmpassword").val('');
        $("#ex3").text("密碼至少大於 8 碼");
      }
      else
      {
        verify_newpasswd = true;
      }

      //如果確認密碼輸入為空
      if ($("#box-confirmpassword").val() == '')
      {
        $("#box-confirmpassword").css({"border": "1px solid #ff0000"});
        $("#ex4").text("未輸入密碼").css({"color": "#ff0000"}); //提醒未輸入確認密碼
      }
      else if($("#box-oldpassword").val() == $("#box-newpassword").val() || $("#box-oldpassword").val() == $("#box-confirmpassword").val())
      {
        $("#box-oldpassword").val('');
        $("#box-newpassword").val('');
        $("#box-confirmpassword").val('');
        $("#ex4").text("不可以與舊密碼一樣");
      }
      else if ($("#box-newpassword").val() != $("#box-confirmpassword").val())
      {
        $("#box-oldpassword").val('');
        $("#box-newpassword").val('');
        $("#box-confirmpassword").val('');
        $("#ex4").text("密碼輸入不一致");
      }
      else
      {
        verify_confirmpassword = true;
      }

      if (verify_account == true && verify_oldpasswd == true && verify_newpasswd == true && verify_confirmpassword == true)
      {
        $.ajax({
          type: "POST", //使用 POST 方式傳送資料
          url: "php/modify_pw.php",
          data: {
            ac: $("#box-account").val(),
            oldpw: $("#box-oldpassword").val(),
            newpw: $("#box-newpassword").val()
          },
          dataType: 'html'
        }).done(function(data){
          //傳送成功的時候
          console.log(data);
          if (data == "yes")
          {
            alert("修改成功請重新登入");
            window.location.href = "php/loginOut.php"; //之後要改路徑
          }
          else
          {
            alert("修改失敗請確認密碼輸入無誤!");
            $("#box-oldpassword").val("");
            $("#box-newpassword").val("");
            $("#box-confirmpassword").val("");
          }
        }).fail(function(jqXHR, textStatus, errorThrown){
          //傳送失敗的時候
          alert("有錯誤產稱請看 consle.log");
          console.log(jqXHR.resoneText);
        });
      }
      
      return false;
  });
})