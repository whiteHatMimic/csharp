//刪除網誌

$(document).on("ready", function(){
  $("a.delete").on("click", function(){
    var c = confirm("您確定要刪除篇網誌嗎"),
        this_tr = $(this).parent().parent();

    if (c)
    {
      $.ajax({
        type: "POST",
        url: "php/del_blog.php",
        data: {
          id: $(this).attr("data-id")
        },
        dataType: 'html'
      }).done(function(data){
        //console.log(data);
        if (data == 'yes')
        {
          alert("刪除成功，點擊確認從列表移除");
          this_tr.fadeOut();
          window.location.reload();
        }
        else
        {
          alert("刪除錯誤" + data);
        }
      }).fail(function(jqXHR, textStatus, errorThrown){
        //失敗的時候
        alert("有錯誤產生，請看 console log");
        console.log(jqXHR.responseText);
      });
    }

    return false;
  });
})
