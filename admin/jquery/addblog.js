$(document).on('ready', function(){
    let count = 0;

    function checkField(title, publish)
    {
        let arr = [title, publish];
        
        $.each(arr, function(index, value){
            let self = value.val();
            if (self === '' | typeof(self) === undefined)
            {
                console.log(index);
                count = count + 1;
            }
        });
    }

    $("#save-chapter").on('click', function(){
        const title = $('input[name=title]');
        const content = CKEDITOR.instances.editor1.getData(); //ckeditor 
        const publish = $('input[name=publish]:checked');
        const username = $('span.username');

        checkField(title, publish);

        if (count == 0)
        {
            $.ajax({
                type: 'POST',
                url: 'php/addblog.php',
                data: {
                    title: title.val(),
                    content: content,
                    publish: publish.val(),
                    username: username.text()
                },
                dataType: 'html'
            }).done(function(data){
                if (data == 'yes')
                {
                    alert("新增成功，請回列表確認");
                    location.assign(location);
                }
                else
                {
                    alert("新增失敗");
                }
            }).fail(function(jqXHR, textStatus, errorThrown){
                alert('有錯誤產生，請看 console.log');
                console.log(jqXHR.responeText);
            });
        }
        else
        {
            count = 0;
            alert("所有欄位接區填寫");
        }

        return false;
    })
})