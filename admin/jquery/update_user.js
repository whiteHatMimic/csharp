$(document).on("ready",function(){
  $("#username").on("input", function(){
    this.value = this.value.replace(/[\\\.\/\*\-\+\~\`\!\_\@\#\$\%\^\&\(\)\{\}\[\]\:\;\"\'\<\>\,\=]/g, "");
    
    if ($(this).val() != '')
    {
      $.ajax({//使用 ajax 傳送資料
        type: "POST", //表單傳送的方式同 form 的 mothed
        url: "php/check_username.php", //目標給哪的檔案，同 form 的 action 屬性
        data:{
          n : $(this).val() //代表要傳的一個 n 變數值為，username 文字方塊
        },
        dataType: 'html' //設定該回應的會是 html 格式
      }).done(function(data){
        //console.log(data);  //確認資料傳送後是否得到資料回應
        //成功的時候回傳資料
        if (data == 'yes')
        {
          //暱稱已存在，把文字方塊變成紅色
          $("#username").css({"border": "1px solid #ff0000"});
          $("#ex1").text("此暱稱已存在，不可新增").css({"color": "#ff0000"});
          $("#save-user").attr('disabled', true).css({"background-color": "#80dfff"});
        }
        else
        {
          $("#username").css({"border": "1px solid #00bfff"});
          $("#ex1").text("");
          $("#save-user").attr('disabled', false).css({"background-color": "#00bfff"});
        }
      }).fail(function(jqXHR, textStatus, errorThrown){
        //失敗的時候
        alert('有錯誤產生，請看 console.log');
        console.log(jqXHR.responeText);
      });
    }
    else
    {
      //不檢查暱稱是否重複
      //如果 input 為空值得話把文字方塊變回藍色
      $("#username").css({"border": "1px solid #00bfff"});
      $("#ex1").text("");
      $("#save-user").attr('disabled', false).css({"background-color": "#00bfff"});
    }
  });

  $("#account").on("input", function(){
    this.value = this.value.replace(/[\\\.\/\*\-\+\~\`\!\_\@\#\$\%\^\&\(\)\{\}\[\]\:\;\"\'\<\>\,\=]/g, "");
    //檢查 account 有沒有重複
    if ($(this).val() != '')
    {
      $.ajax({
        type: "POST",
        url: "php/check_account.php",
        data:{
          e: $(this).val()
        },
        dataType: 'html'
      }).done(function(data){
        console.log(data);
        //傳送成功的時候
        if (data == 'yes')
        {
          //account 已存在，把文字方塊變紅色
          $("#account").css({"border": "1px solid #ff0000"});
          $("#ex2").text("此帳號已註冊，請使用別的帳號").css({"color": "#ff0000"});
          $("#save-user").attr('disabled', true).css({"background-color": "#80dfff"});
        }
        else
        {
          $("#account").css({"border": "1px solid #00bfff"});
          $("#ex2").text("");
          $("#save-user").attr('disabled', false).css({"background-color": "#00bfff"});
        }
      }).fail(function(jqXHR, textStatus, errorThrown){
        //傳送失敗的時候
        alert("有錯誤產稱請看 consle.log");
        console.log(jqXHR.resoneText);
      });
    }
    else
    {
      //不檢查 account 是否重複
      //如果 input 為空值得話把文字方塊變回藍色，提示文字變為空，把按鈕啟用
      $("#account").css({"border": "1px solid #00bfff"});
      $("#ex2").text("");
      $("#save-user").attr('disabled', false).css({"background-color": "#00bfff"});
    }
  });

  //儲存更新資料
  $("#save-user").on("click", function(){
    var ver_English = /[A-Za-z]/g; //英文輸入驗證
    var ver_Number = /[0-9]/g; //英文輸入驗證
    var ver_Symbol = /[\\\.\/\*\-\+\~\`\!\_\@\#\$\%\^\&\(\)\{\}\[\]\:\;\"\'\<\>\,\=]/g; //暱稱驗證
    var fun = false, fac = false, fid = false, fst = false;

    //username
    if ($("#username").val() == '') //驗證 username 是否為空白
    {
      $("#username").css({"border": "1px solid #ff0000"});
      $("#ex1").text("未輸入暱稱").css({"color": "#ff0000"});
    }
    else if (ver_Symbol.test($("#username").val()))
    {
      $("#username").css({"border": "1px solid #ff0000"});
      $("#ex1").text("禁止輸入符號").css({"color": "#ff0000"});
    }
    else if ($("#username").val().length < 3) //驗證 username 字元長度是否少於 4 字元
    {
      $("#username").css({"border": "1px solid #ff0000"});
      $("#ex1").text("匿稱輸入起碼超過 3 個字").css({"color": "#ff0000"});
    }
    else if ($("#username").val().length >= 15)
    {
      $("#username").css({"border": "1px solid #ff0000"});
      $("#ex1").text("匿稱輸入起碼超過字數限制字元").css({"color": "#ff0000"});
    }
    else
    {
      $("#username").css({"border": "1px solid #00bfff"});
      $("#ex1").text("");
      fun = true;
    }

    //account
    if ($("#account").val() == '') //驗證 account 是否為空白
    {
      $("#account").css({"border": "1px solid #ff0000"});
      $("#ex2").text("未輸入帳號!").css({"color": "#ff0000"});
    }
    else if (ver_English.test($("#account").val()) == false && ver_Number.test($("#account").val()) == false) //帳號
    {
      $("#account").css({"border": "1px solid #ff0000"});
      $("#ex2").text("帳號必須是英文數字組合!").css({"color": "#ff0000"});
    }
    else if (!ver_Symbol.test("#account"))
    {
      $("#account").css({"border": "1px solid #ff0000"});
      $("#ex2").text("不可輸入特殊符號!").css({"color": "#ff0000"});
    }
    else if ($("#account").val().length < 8) //驗證 Email 是否符合格式輸入
    {
      $("#account").css({"border": "1px solid #ff0000"});
      $("#ex2").text("帳號輸入起碼大於 8 個字元").css({"color": "#ff0000"});
    }
    else if ($("#account").val().length >= 20)
    {
      $("#account").css({"border": "1px solid #ff0000"});
      $("#ex2").text("帳號輸入超出字數限制字元").css({"color": "#ff0000"});
    }
    else
    {
      $("#account").css({"border": "1px solid #00bfff"});
      $("#ex2").text("");
      fac = true;
    }

    if (typeof($("input[name=identity]:checked").val()) === "undefined")
    {
      $("#ex3").text("請選擇身分角色").css({"color": "#ff0000"});
    }
    else
    {
      $("#ex3").text("");
      fid = true;
    }

    if (typeof($("input[name=state]:checked").val()) === "undefined")
    {
      $("#ex4").text("請選擇狀態").css({"color": "#ff0000"});
    }
    else
    {
      $("#ex4").text("");
      fst = true;
    }

    if (fun == true && fac == true && fid == true && fst == true)
    {
      $.ajax({
        type: "POST",
        url: "php/update_user.php",
        data: {
          user_id: $("#user_id").val(),
          username: $("#username").val(),
          account: $("#account").val(),
          identity: $("input[name=identity]:checked").val(),
          state: $("input[name=state]:checked").val(),
        },
        dataType: 'html'
      }).done(function(data){
        console.log(data);
        if (data == 'yes')
        {
          alert("更新成功，請查看列表確認");
          location.assign(location);
        }
        else
        {
          alert("修改失敗");
        }
      }).fail(function(jqXHR, textStatus, errorThrown){
        //失敗的時候
        alert('有錯誤產生，請看 console.log');
        console.log(jqXHR.responeText);
      });
    }

    return false;
  });

});
