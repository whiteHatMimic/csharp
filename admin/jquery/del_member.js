$(document).on("ready",function(){
  $("a.delete").click(function(){
    var this_tr = $(this).parent().parent();

    $.ajax({
      type: 'POST',
      url: 'php/del_user.php',
      data: {
        id: $(this).attr("data-id")
      },
      dataType: 'html'
    }).done(function(data){
      //console.log(data);
      if (data == 'yes')
      {
        alert("刪除成功，請回列表確認");
        this_tr.fadeOut();
        location.assign(location);
      }
      else
      {
        alert("刪除錯誤");
        console.log(data);
      }
    }).fail(function(jqXHR, textStatus, errorThrown){
      //失敗的時候
      alert("有錯誤產生，請看 console log");
      console.log(jqXHR.responseText);
    });
  });
})
