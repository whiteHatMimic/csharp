<?php
  require_once 'php/db.php';
  require_once 'php/functions.php';

  if (!isset($_SESSION['is_login']) || !$_SESSION['is_login'])
  {
    header("Location: login.php");
  }

  //取得所有文章資料
  $dates1 = get_all_chapter();

  //取得目前選取的 id 且可逕行修改動作
  $dates2 = get_chapter_id($_GET['cha']);

  //如果id 不存在就跳出
  if (is_null($dates2))
  {
    header("Location: chapter.php");
  }
?>

<!DOCTYPE html>
<html>
  <head>
    <title>C# School</title>
    <meta http-equiv="X-UA-Compatible" content="chrome=1" charset="utf-8">
    <link rel="shortcut icon" href="image/logo.ico">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/menu.css">
    <link rel="stylesheet" href="css/chapteredit.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!--[if lt IE 7.]>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
    <![endif]-->
    <script src="ckeditor/ckeditor.js"></script>
    <script src="jquery/switch_Picture.js"></script>
    <script src="jquery/update_chapter.js"></script>
  </head>
  <body>
    <div id="uname">
      <span class="username"><b><?php echo $_SESSION['is_username'];?></b></span>
      <span class="Mpasswd"><a href="modify_pw.php"><b>修改密碼</b></a></span>
      <span class="loginOut"><a href="php/loginOut.php"><b>登出</b></a></span>
    </div>

    <!--top-->
    <div id="top">
      <span>
        <span class="CShool">
          <a href="index.php"><b>C# School</b></a>
        </span>
        <span class="com">
          <a href="index.php"><b>.com</b></a>
        </span>
        <span>
          <span class="bottom"><b>後台</b></span>
        </span>
      </span>
    </div>

    <!--menu-->
    <?php
      include_once 'menu.php';
    ?>

    <div id="container1">
      <div class="con-scope">
        <!-- 功能選項 -->
        <div class="box-tool">
          <div class="tool_menu">
            <a class="add-un" href="chapteradd.php">新增文章</a>
          </div>
        </div>

        <!-- 章節清單 -->
        <div class="con-list-scope">
          <div class="con-chapter-list">
            <!-- 會員清單 -->
            <div class="con-list-chX">
              <div class="con-user-listX">
                <h2>列表清單</h2>
                <?php if (!empty($dates1)):?>
                  <?php foreach($dates1 as $key => $row):?>
                    <a class="con-index" href="./chapteredit.php?cha=<?php echo $row['id']?>" <?php echo ($row['title'] == $dates2['title'])? "style='background-color: #00bfff; color: #ffffff'":""?>>
                      <div class="sub_section">
                        <span class="box-u"><?php echo $row['title'];?></span>
                      </div>
                    </a>
                  <?php endforeach;?>
                <?php endif;?>
              </div>
            </div>

            <!-- 觀看會員資料建立 -->
            <div class="con-all-user-date">
              <div class="con-basic">
                <div class="con-basic-date">
                  <span class="basic">基本資訊</span>
                  <span class="close"><a href="index.php"><img src="image/close.png"></a></span>
                </div>
              </div>

              <!--觀看會員資料-->
              <div class="con-user">
                <div class="con-user-date">
                  <div class="contents">
                    <div class="sub_section_id">
                      <input id="sub_section_id" type="hidden" name="sub_section_id" value="<?php echo $dates2['id'];?>">
                    </div>
                  </div>

                  <div class="contents">
                    <div class="ch_title">
                      <span class="con-title">標題</span>
                      <input type="text" name="title" value="<?php echo $dates2['title'];?>">
                    </div>
                  </div>

                  <div class="contents">
                    <div class="content">
                      <span class="con-title">內容</span>
                      <textarea id="editor1" name="sub_ch_content">
                        <?php echo $dates2['content'];?>
                      </textarea>
                      <script>
                        CKEDITOR.replace('editor1', {
                          height: 350,
                          removePlugins: 'resize',
                          filebrowserBrowseUrl : 'filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
                          filebrowserUploadUrl : 'filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
                          filebrowserImageBrowseUrl : 'filemanager/dialog.php?type=1&editor=ckeditor&fldr='
                        });
                      </script>

                      <span id="ex3"></span>
                    </div>
                  </div>

                  <div class="contents">
                    <div class="content">
                      <span class="con-title">建立人員</span>
                      <span><?php echo $dates2['build_Staff'];?></span>
                    </div>
                  </div>

                  <div class="contents">
                    <div class="content">
                      <span class="con-title">最後更新人員</span>
                      <span><?php echo $dates2['last_modified_Staff'];?></span>
                    </div>
                  </div>

                  <div class="contents">
                    <div class="publish">
                      <span class="con-title">發佈狀態</span>
                      <input id="con-normal" class="con-radio" type="radio" name="publish" value="1" <?php echo ($dates2['publish'] == 1)? "checked":""?>>
                      <label for="con-normal">發佈</label>
                      <input id="con-disabled" class="con-radio" type="radio" name="publish" value="0" <?php echo ($dates2['publish'] == 0)? "checked":""?>>
                      <label for="con-disabled">不發佈</label>
                      <span id="ex4"></span>
                    </div>
                  </div>

                  <div class="contents">
                    <div class="Adate">
                      <span class="con-title">建立日期</span>
                      <span><?php echo $dates2['addDate'];?></span>
                    </div>
                  </div>

                  <div class="contents">
                    <div class="Mdate">
                      <span class="con-title">最後修改日期</span>
                      <span><?php echo $dates2['Mdate']?></span>
                    </div>
                  </div>
                </div>
              </div>

              <!--儲存資料或離開-->
              <div class="con-tool">
                <div class="con-button">
                  <button id="save-chapter" class="con-button-blue" type="submit" name='save'>儲存</button>
                  <button id="close-chapter" class="con-button-gray" type="submit" name='close'><a href="index.php">取消</a></button>
                </div>
              </div>
            </div><!--test2 exit-->
          </div><!--con-ac-liset exit-->
        </div><!--con-list exit-->
      </div><!--con-scope exit-->
    </div><!--container exit-->

    <!--聯絡方式-->
    <div id="contact">
      <h2 class="title">聯繫我們</h2>

      <!--連結google社群-->
      <span>
        <a href="https://plus.google.com/u/0/communities/109335508485514749844" target="_blank">
          <img class="box-g" src="image/box-google1.png">
        </a>
      </span>

      <!--連結facebook社群-->
      <span>
        <a href="https://www.facebook.com/groups/534251500275740/" target="_blank">
          <img class="box-f" src="image/box-facebook1.png">
        </a>
      </span>

      <!--連結line社群-->
      <span>
        <a href="http://line.me/ti/p/%40ino5143k" target="_blank">
          <img class="box-l" src="image/box-line1.png">
        </a>
      </span>

      <span>
        <p>Copyright &copy; <?php echo date("Y")?> C#-School. All rights reserved</p>
      </span>
    </div>
  </body>
</html>
