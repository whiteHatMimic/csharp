<?php
  require_once 'php/db.php';
  require_once 'php/functions.php';

  if (!isset($_SESSION['is_login']) && $_SESSION['is_login'])
  {
    header("Location: login.php");
  }

  //取得所有網誌
  $dates = get_all_blog();
?>

<!DOCTYPE html>
<html>
  <head>
    <title>C# School</title>
    <meta http-equiv="X-UA-Compatible" content="chrome=1" charset="utf-8">
    <link rel="shortcut icon" href="image/logo.ico">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/menu.css">
    <link rel="stylesheet" href="css/blog.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
    <script src="http://code.jquery.com/jquery-latest.min.js"></script>
    <!--[if lt IE 7.]>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
    <![endif]-->
    <script src="jquery/switch_Picture.js"></script>
    <script src="jquery/del_blog.js"></script>
  </head>
  <body>
    <div id="uname">
      <span class="username"><b><?php echo $_SESSION['is_username'];?></b></span>
      <span class="Mpasswd"><b><a href="./modify_user.php">修改密碼</a></b></span>
      <span class="loginOut"><b><a href="php/loginOut.php">登出</a></b></span>
    </div>

    <!--top-->
    <div id="top">
      <span>
        <span class="CShool">
          <a href="index.php"><b>C# School</b></a>
        </span>
        <span class="com">
          <a href="index.php"><b>.com</b></a>
        </span>
        <span>
          <span class="bottom"><b>後台</b></span>
        </span>
      </span>
    </div>

    <!--menu-->
    <?php
      include_once 'menu.php';
    ?>

    <div id="container1">
      <div class="con-scope">
        <!-- 功能選項 -->
        <div class="box-tool">
          <div class="con-ac">
            <div class="navbar">
              <div class="dropdown">
                <a class="add-un" href="./blogadd.php">新增網誌</a>
              </div>
            </div>
          </div>
        </div>

        <!-- contents -->
        <div class="user-content">
          <div class="content">
            <table>
              <tbody>
                <tr class="box-black">
                  <td></td>
                  <td>標題</td>
                  <td>內容</td>
                  <td>發佈狀況</td>
                  <td>新增時間</td>
                  <td>管理動作</td>
                </tr>
              </tbody>

              <tbody class="blog-content">
                <?php if(!empty($dates)):?>
                  <?php foreach($dates as $key=>$row):?>
                    <?php
                      //將資料庫的內容儲存到 $content 變數中
                      $content = $row['content'];

                      //去除標籤或特殊符號
                      $content = filter_var($content, FILTER_SANITIZE_STRING);

                      //取得 20 個字
                      $content = mb_substr($content, 0, 20, "UTF-8");
                    ?>

                    <tr class="box-gray" <?php echo ($key % 2 != 0)?"style='background-color:#d9d9d9'":"style='background-color:#ffffff'"?>>
                      <th class="blognumber"><?php echo $key + 1;?></th>
                      <th><?php echo $row['title'];?></th>
                      <th><?php echo $content?></th>
                      <th><?php echo ($row['publish'])?"發布":"不發佈";?></th>
                      <th><?php echo $row['addDate'];?></th>
                      <th>
                        <a class="edit" href="./blogedit.php?blo=<?php echo $row['id']?>">編輯</a>
                        <a class="delete" href="javascript:void(0);" data-id="<?php echo $row['id'];?>">刪除</a>
                      </th>
                    </tr>
                  <?php endforeach;?>
                <?php else:?>
                  <tr>
                    <th colspan="6">無資料</th>
                  </tr>
                <?php endif;?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <!--聯絡方式-->
    <div id="contact">
      <h2 class="title">聯繫我們</h2>

      <!--連結google社群-->
      <span>
        <a href="https://plus.google.com/u/0/communities/109335508485514749844" target="_blank">
          <img class="box-g" src="image/box-google1.png">
        </a>
      </span>

      <!--連結facebook社群-->
      <span>
        <a href="https://www.facebook.com/groups/534251500275740/" target="_blank">
          <img class="box-f" src="image/box-facebook1.png">
        </a>
      </span>

      <!--連結line社群-->
      <span>
        <a href="http://line.me/ti/p/%40ino5143k" target="_blank">
          <img class="box-l" src="image/box-line1.png">
        </a>
      </span>

      <span>
        <p>Copyright &copy; <?php echo date("Y")?> C#-School. All rights reserved</p>
      </span>
    </div>
  </body>
</html>
