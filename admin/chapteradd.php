<?php
  require_once 'php/db.php';
  require_once 'php/functions.php';

  if (!isset($_SESSION['is_login']) || !$_SESSION['is_login'])
  {
    header("Location: login.php");
  }

  //取得所有文章資料
  $dates1 = get_all_chapter();
?>

<!DOCTYPE html>
<html>
  <head>
    <title>C# School</title>
    <meta http-equiv="X-UA-Compatible" content="chrome=1" charset="utf-8">
    <link rel="shortcut icon" href="image/logo.ico">
    <link rel="stylesheet" href="css/reset.css">
    <link rel="stylesheet" href="css/menu.css">
    <link rel="stylesheet" href="css/chapteradd.css">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!--[if lt IE 7.]>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv-printshiv.min.js"></script>
    <![endif]-->
    <script src="ckeditor/ckeditor.js"></script>
    <script src="jquery/switch_Picture.js"></script>
    <script src="jquery/add_blog.js"></script>
  </head>
  <body>
    <div id="uname">
      <span class="username"><b><?php echo $_SESSION['is_username'];?></b></span>
      <span class="Mpasswd"><b><a href="modify_user.php">修改密碼</a></b></span>
      <span class="loginOut"><b><a href="php/loginOut.php">登出</a></b></span>
    </div>

    <!--top-->
    <div id="top">
      <span>
        <span class="CShool">
          <a href="index.php"><b>C# School</b></a>
        </span>
        <span class="com">
          <a href="index.php"><b>.com</b></a>
        </span>
        <span>
          <span class="bottom"><b>後台</b></span>
        </span>
      </span>
    </div>

    <!--menu-->
    <?php
      include_once 'menu.php';
    ?>

    <div id="container1">
      <div class="con-scope">
        <!-- 功能選項 -->

        <div class="box-tool">
          <div class="con-ac">
            <div class="navbar">
              <div class="dropdown">
                <a class="add-un" href="./chapteradd.php">新增文章</a>
              </div>
            </div>
          </div>
        </div>

        <!-- 會員清單內容 -->
        <div class="con-list-scope">
          <div class="con-user-list">
            <!-- 會員清單 -->
            <div class="con-list-acX">
              <div class="con-user-listX">
                <h2>列表清單</h2>
                <?php if (!empty($dates1)):?>
                  <?php foreach($dates1 as $key => $row):?>
                    <a class="con-index" href="./chapteredit.php?cha=<?php echo $row['id']?>">
                      <div class="sub_section">
                        <span class="box-u"><?php echo $row['title'];?></span>
                      </div>
                    </a>
                  <?php endforeach;?>
                <?php endif;?>
              </div>
            </div>

            <!-- 觀看會員資料建立 -->
            <div class="con-all-user-date">
              <div class="con-basic">
                <div class="con-basic-date">
                  <span class="basic">基本資訊</span>
                  <span><a href="index.php"><img src="image/close.png"></a></span>
                </div>
              </div>

              <!--觀看會員資料-->
              <div class="con-user">
                <div class="con-user-date">
                  <div class="contents">
                    <div class="ch_title">
                      <span class="con-title">標題</span>
                      <input type="text" name="title">
                    </div>
                  </div>

                  <div class="contents">
                    <div class="content">
                      <span class="con-title">內容</span>
                      <textarea id="editor1" name="sub_ch_content"></textarea>
                      <script>
                        CKEDITOR.replace('editor1', {
                          height: 350,
                          removePlugins: 'resize',
                          filebrowserBrowseUrl : 'filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
                          filebrowserUploadUrl : 'filemanager/dialog.php?type=2&editor=ckeditor&fldr=',
                          filebrowserImageBrowseUrl : 'filemanager/dialog.php?type=1&editor=ckeditor&fldr='
                        });
                      </script>

                      <span id="ex3"></span>
                    </div>
                  </div>

                  <div class="contents">
                    <div class="publish">
                      <span class="con-title">發佈狀態</span>
                      <input id="con-normal" class="con-radio" type="radio" name="publish" value="1" checked>
                      <label for="con-normal">發布</label>
                      <input id="con-disabled" class="con-radio" type="radio" name="publish" value="0">
                      <label for="con-disabled">不發布</label>
                      <span id="ex4"></span>
                    </div>
                  </div>
                </div>
              </div>

              <!--儲存資料或離開-->
              <div class="con-tool">
                <div class="con-button">
                  <button id="save-chapter" class="con-button-blue" type="submit" name='save'>儲存</button>
                  <button id="close-chapter" class="con-button-gray"><a href="index.php">取消</a></button>
                </div>
              </div>
            </div><!--test2 exit-->
          </div><!--con-ac-liset exit-->
        </div><!--con-list exit-->
      </div><!--con-scope exit-->
    </div><!--container exit-->

    <!--聯絡方式-->
    <div id="contact">
      <h2 class="title">聯繫我們</h2>

      <!--連結google社群-->
      <span>
        <a href="https://plus.google.com/u/0/communities/109335508485514749844" target="_blank">
          <img class="box-g" src="image/box-google1.png">
        </a>
      </span>

      <!--連結facebook社群-->
      <span>
        <a href="https://www.facebook.com/groups/534251500275740/" target="_blank">
          <img class="box-f" src="image/box-facebook1.png">
        </a>
      </span>

      <!--連結line社群-->
      <span>
        <a href="http://line.me/ti/p/%40ino5143k" target="_blank">
          <img class="box-l" src="image/box-line1.png">
        </a>
      </span>

      <span>
        <p>Copyright &copy; <?php echo date("Y")?> C#-School. All rights reserved</p>
      </span>
    </div>
  </body>
</html>

<?php
  /*$newTime = date("Y-m-d H:i:s"); //現在時間
  $username = $_SESSION['is_username'];

  if (isset($_POST['save']))
  {
    if (isset($_POST['title']) && !empty($_POST['title'])) //章節標題
    {
      $title = $_POST['title'];
    }

    if (isset($_POST['sub_ch_content']) && !empty($_POST['sub_ch_content'])) //章節內容
    {
      $content = $_POST['sub_ch_content'];
    }

    if (isset($_POST['publish']) && !empty($_POST['publish'])) //發佈狀況
    {
      $publish = $_POST['publish'];
    }

    $result = null;

    $sql = "INSERT INTO `chapter_list` (`title`, `content`, `build_Staff`, `last_modified_Staff`, `publish`, `addDate`, `Mdate`)
            VALUES ('{$title}', '{$content}', '$username', '$username', '{$publish}', '{$newTime}', '{$newTime}')";

    $query = mysqli_query($_SESSION['link'], $sql);

    if ($query)
    {
      if(mysqli_affected_rows($_SESSION['link']) == 1)
      {
        $result = true;
        mysqli_close($_SESSION['link']);
        echo "<script>alert('更新新成功，請回列表確認!')</script>";
        echo "<script>location.assign(location)</script>";
      }
      else
      {
        echo "<script>alert('更新錯誤');</script>";
      }
    }
    else
    {
      echo '{$sql}語法請求失敗' . mysqli_error($_SESSION['link']);
    }
  }
  else
  {
    return false;
  }*/
?>
